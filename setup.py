from setuptools import setup
from xdyngrpc import __version__

description = "Python package for proto interface files"
with open("Readme.md", "r", encoding="utf-8") as fid:
    long_description = fid.read()
with open("requirements.txt", "r", encoding="utf-8") as fid:
    requirements = fid.readlines()
    requirements = [line for line in requirements if not line.startswith("#")]

setup(
    name="xdyngrpc",
    version=__version__,
    description=description,
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Intended Audience :: Developers",
        "Intended Audience :: Information Technology",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved ::Eclipse Public License - v 2.0",
        "Environment :: Console",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    author="Sirehna",
    author_email="sirehna@sirehna.com",
    url="https://sirehna.com",
    license="Eclipse Public License - v 2.0",
    packages=["xdyngrpc"],
    package_data={},
    python_requires=">=3.7",
    zip_safe=False,  # https://mypy.readthedocs.io/en/latest/installed_packages.html
    install_requires=requirements,
    extras_require={},
    entry_points={
        "console_scripts": [
            "xdyngrpc=xdyngrpc.__main__:main",
        ]
    },
)
