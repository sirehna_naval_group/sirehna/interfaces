# Protobuf interfaces for use with [xdyn](https://gitlab.com/sirehna_naval_group/sirehna/xdyn)

[![License](https://img.shields.io/badge/License-EPL%202.0-blue.svg)](https://opensource.org/licenses/EPL-2.0)

## Description

This repository contains protobuf files which are used by xdyn to communicate
with force models, wave models & controllers, but also offers an interface to
xdyn itself (model exchange and co-simulation).

All `proto` files are located in [`proto`](./proto) directory.
Directory [`xdyngrpc`](./xdyngrpc) contains the Python package to use gRPC interfaces.
Directory [`examples`](./examples) contains Python and C++ examples, using the `proto` files.

Examples are `docker-compose` files that describe the services to launch for
the demo purpose.

## Getting started

A simple `make` command will build everything. One can have a look at the [`Makefile`](./Makefile)
to see the various targets.
