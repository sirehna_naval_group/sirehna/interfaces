"""gRPC sample client for controllers."""

import logging
from typing import Dict, List

import grpc

from .._proto import controller_pb2, controller_pb2_grpc
from .controller import StatesEuler

SERVICE_NAME = "controller-client"

logging.basicConfig(
    format="%(asctime)s,%(msecs)d ["
    + SERVICE_NAME
    + "] - %(levelname)-4s [%(filename)s:%(lineno)d] %(message)s",
    datefmt="%d-%m-%Y:%H:%M:%S",
)
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def to_grpc_states(states: StatesEuler) -> controller_pb2.ControllerStatesEuler:
    """Convert Python dataclass to protobuf."""
    ret = controller_pb2.ControllerStatesEuler()
    ret.t = states.t
    ret.x = states.x
    ret.y = states.y
    ret.z = states.z
    ret.u = states.u
    ret.v = states.v
    ret.w = states.w
    ret.p = states.p
    ret.q = states.q
    ret.r = states.r
    ret.phi = states.phi
    ret.theta = states.theta
    ret.psi = states.psi
    return ret


class Controller:
    """This class wraps the gRPC calls for use with "normal" python types."""

    def __init__(self, channel, parameters: str, t0: float = 0):
        """Constructor."""
        self.stub = controller_pb2_grpc.ControllerStub(channel)
        self.date_of_first_callback = None
        self.setpoint_names = None
        self.angle_representation = None
        self.set_parameters(parameters, t0)

    def set_parameters(self, yaml_string: str, t0: float) -> str:
        """Set the controller's YAML parameters."""
        request = controller_pb2.SetParametersRequest(parameters=yaml_string, t0=t0)
        response = self.stub.set_parameters(request)
        self.date_of_first_callback = response.date_of_first_callback
        self.setpoint_names = response.setpoint_names
        self.angle_representation = response.angle_representation

    def get_commands_euler_321(
        self,
        states: StatesEuler,
        dstates_dt: StatesEuler,
        setpoints: List[float],
    ) -> Dict[str, float]:
        """Calculates the controller outputs (the commands)."""
        request = controller_pb2.ControllerRequestEuler()
        grpc_states = to_grpc_states(states)
        grpc_dstates_dt = to_grpc_states(dstates_dt)
        request.states.CopyFrom(grpc_states)
        request.dstates_dt.CopyFrom(grpc_dstates_dt)
        request.setpoints[:] = setpoints
        response = self.stub.get_commands_euler_321(request)
        return (response.commands, response.next_call)


def test_controller(address: str):
    """Launch the server & run some gRPC calls."""
    LOGGER.info(f"Testing controller at {address}")
    with grpc.insecure_channel(address) as channel:
        yaml_parameters = """
            command: beta
            dt: 0.5
            setpoint: rpm_co
            state weights:
                x: 1
                y: -1
            gains:
                Kp: 2.5
                Ki: 0.1
                Kd: 0.314
        """
        Kp = 2.5
        Ki = 0.1
        Kd = 0.314
        dt = 0.5
        pid = Controller(channel, yaml_parameters)
        # First time step
        error = 0.05
        rpm_co = 5
        states = StatesEuler(
            x=2 * rpm_co,
            y=rpm_co + error,
            z=300.0,
            u=1.0,
            v=2.0,
            w=3.0,
            p=4.0,
            q=5.0,
            r=6.0,
        )
        dstates_dt = StatesEuler()

        first_expected_command = Kp * error
        assert (
            abs(
                pid.get_commands_euler_321(states, dstates_dt, [rpm_co])[0]["beta"]
                - first_expected_command
            )
            < 1e-6
        )

        # Second time step
        error2 = -0.03
        rpm_co = 1.5
        states.x = 2 * rpm_co
        states.y = rpm_co + error2
        second_expected_command = Kp * error2 + Ki * error2 * dt + Kd * (error2 - error) / dt
        assert (
            abs(
                pid.get_commands_euler_321(states, dstates_dt, [rpm_co])[0]["beta"]
                - second_expected_command
            )
            < 1e-6
        )

        # Third time step
        error3 = 0.01
        rpm_co = 5.5
        states.x = 2 * rpm_co
        states.y = rpm_co + error3
        third_expected_command = (
            Kp * error3 + Ki * (error2 * dt + error3 * dt) + Kd * (error3 - error2) / dt
        )

        assert (
            abs(
                pid.get_commands_euler_321(states, dstates_dt, [rpm_co])[0]["beta"]
                - third_expected_command
            )
            < 1e-6
        )

        LOGGER.info("ALL DONE!")


def test_python_controller():
    """Test Python controller"""
    test_controller("python-controller:9002")


def test_cpp_controller():
    """Test C++ controller"""
    test_controller("cpp-controller:9002")


if __name__ == "__main__":
    LOGGER.info("Starting client")
    test_python_controller()
    test_cpp_controller()
