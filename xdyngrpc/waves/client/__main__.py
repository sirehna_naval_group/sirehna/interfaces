"""gRPC sample client for wave models."""

from . import LOGGER, run

if __name__ == "__main__":
    LOGGER.info("Starting client")
    run()
