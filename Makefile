.PHONY: all docker-images

ROOT_DIRECTORY=xdyngrpc
DOCKER_IMAGE_PYTHON_RUN=$(ROOT_DIRECTORY)-python
DOCKER_IMAGE_PYTHON_BUILD=$(ROOT_DIRECTORY)-python-build

all: build
	@make -C examples

# Display the name and comment of each target followed by ' ##'
help: ## Display help and exit.
	@echo 'usage: make [target]'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

build: ## Generate proto code, the docker image and the wheel for xdyngrpc
build: build_python

build_python: \
	python_generated_code \
	python_lint \
	python_docker_image \
	python_docker_image_test \
	python_docker_wheel

LINE_LENGTH:=100
PROJECT_NAME:=xdyngrpc
DOCKER_RUN:=docker run --rm -u $(shell id -u ):$(shell id -g) -v $(shell pwd):/work -w /work
PYTHON_BUILD:=$(DOCKER_RUN) $(DOCKER_IMAGE_PYTHON_BUILD)

python_format: ## Run isort and black to format code
	@$(PYTHON_BUILD) -m isort --line-length ${LINE_LENGTH} --profile black ${PROJECT_NAME} --skip ${PROJECT_NAME}/_proto
	@$(PYTHON_BUILD) -m black --line-length ${LINE_LENGTH} ${PROJECT_NAME}/* --exclude ${PROJECT_NAME}/_proto

python_lint: ## Check code with isort, black and pylint to identify any problem
	$(PYTHON_BUILD) -m isort --line-length ${LINE_LENGTH} --profile black --check ${PROJECT_NAME} --skip ${PROJECT_NAME}/_proto
	$(PYTHON_BUILD) -m black --line-length ${LINE_LENGTH} --check ${PROJECT_NAME}/* --exclude ${PROJECT_NAME}/_proto
	$(PYTHON_BUILD) -m pylint ${PROJECT_NAME}/* \
		--disable=invalid-name,unnecessary-lambda-assignment,no-member,duplicate-code \
		--ignore ${PROJECT_NAME}/_proto \
		|| true


clean:
	@rm -rf $(ROOT_DIRECTORY)/_proto
	@rm -rf $(ROOT_DIRECTORY).egg-info
	@rm -rf xdyngrpc*.whl
	@rm -rf build dist
	@make -C examples clean


python_generated_code: \
	python_generated_code_generator \
	python_generated_proto_code

python_generated_code_generator:
	@docker build -f Dockerfile.python-build -t $(DOCKER_IMAGE_PYTHON_BUILD) .

protos_abs_files=$(wildcard proto/*.proto)

python_generated_proto_code: $(protos_abs_files)
	@mkdir -p $(ROOT_DIRECTORY)/_proto
	@touch $(ROOT_DIRECTORY)/_proto/__init__.py
	$(DOCKER_RUN) $(DOCKER_IMAGE_PYTHON_BUILD) \
		-m grpc_tools.protoc \
		-I proto \
		--python_out=/work/$(ROOT_DIRECTORY)/_proto/. \
		--grpc_python_out=/work/$(ROOT_DIRECTORY)/_proto/. \
		$^
	$(DOCKER_RUN) --entrypoint sed $(DOCKER_IMAGE_PYTHON_BUILD) \
		-i -E 's/^import.*_pb2/from . \0/' \
		$(ROOT_DIRECTORY)/_proto/*.py

python_docker_wheel: ## Creata a Wheel package
	$(DOCKER_RUN) $(DOCKER_IMAGE_PYTHON_BUILD) setup.py bdist_wheel
	@mv dist/xdyngrpc*.whl .

python_docker_image:
	@docker build --pull -t $(DOCKER_IMAGE_PYTHON_RUN) -f Dockerfile.python .
	@docker run --rm $(DOCKER_IMAGE_PYTHON_RUN)

python_docker_image_test:
	@docker run --rm --entrypoint nose2 $(DOCKER_IMAGE_PYTHON_RUN)
