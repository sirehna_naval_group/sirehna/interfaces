#include <cmath>
#include <iostream>

#include <grpcpp/grpcpp.h>
#include "controller.grpc.pb.h"
#include "controller.pb.h"
#include "Controller.hpp"
#include "FromGrpc.hpp"
#include "ToGrpc.hpp"

#define QUOTE_ME(x) #x
#define QUOTE(x) QUOTE_ME(x)
#define COUT(val) std::cout << "in file " << __FILE__ << ":" << __LINE__ << " " << #val << " = " << val << std::endl;
#define CERR(val) std::cerr << "in file " << __FILE__ << ":" << __LINE__ << " " << #val << " = " << val << std::endl;
#define THROW(function, exception, message)\
    std::stringstream __ss;\
    __ss << message;\
    exception exc(__ss.str(), __FILE__, function, __LINE__);\
    throw exc;

#include <sstream>

Exception::Exception(const std::string& message, const std::string& file, const std::string& function, const unsigned int line):
        full_message(""),
        short_message(message)
{
    std::stringstream ss;
    ss << "In file " << file << ", line " << line << ", function " << function << ": " << short_message;
    full_message = ss.str();
}

Exception::Exception(const Exception& rhs):
        full_message(rhs.full_message),
        short_message(rhs.short_message)
{
}

Exception::~Exception () throw ()
{
}

const char* Exception::what() const throw()
{
    return full_message.c_str();
}

std::string Exception::get_message() const
{
    return short_message;
}
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& m)
{
    os << "[";
    if (not(m.empty())) os << m.front();
    for (size_t i = 1 ; i < m.size() ; ++i)
    {
        os << ", " << m.at(i);
    }
    os << "]";
    return os;
}

template <typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::map<T,U>& m)
{
    os << "{";
    if (not(m.empty())) os << m.begin()->first << ": " << m.begin()->second;
    for (auto it = m.begin() ; it != m.end() ; ++it)
    {
        if (it!=m.begin()) os << ", " << it->first << ": " << it->second;
    }
    os << "}";
    return os;
}

template <typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::pair<T,U>& m)
{
    os << "(" << m.first << "," << m.second << ")";
    return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& m)
{
    os << "{";
    if (not(m.empty())) os << *m.begin();
    for (auto it = m.begin() ; it != m.end() ; ++it)
    {
        if (it!=m.begin()) os << ", " << *it;
    }
    os << "}";
    return os;
}

Controller_::Controller_() : dt(0), t0(0)
{}

EulerController_::EulerController_() : Controller_()
{}

QuaternionController_::QuaternionController_() : Controller_()
{}

void Controller_::set_dt(const double dt_)
{
    dt = dt_;
}

void Controller_::set_t0(const double t0_)
{
    t0 = t0_;
}

double Controller_::get_date_of_next_call(const double t) const
{
    const int current_index = (int)std::round((t - t0) / dt);
    return t0 + (current_index + 1) * dt;
}

std::map<std::string,double> EulerController_::get_commands_quaternion(const QuaternionStates_&, const QuaternionStates_&, const std::vector<double>&)
{
    THROW(__PRETTY_FUNCTION__, Exception, "Calling get_commands_quaternion on a EulerController: method set_parameters says that this is a quaternion controller but it derives from EulerController.");
    return std::map<std::string, double>();
}

AngleRepresentation_ EulerController_::get_angle_representation() const
{
    return AngleRepresentation_::EULER_321;
}

AngleRepresentation_ QuaternionController_::get_angle_representation() const
{
    return AngleRepresentation_::QUATERNION;
}

std::map<std::string,double> QuaternionController_::get_commands_euler_321(const EulerStates_&, const EulerStates_&, const std::vector<double>&)
{
    THROW(__PRETTY_FUNCTION__, Exception, "Calling get_commands_euler_321 on a QuaternionController: method set_parameters says that this is a euler controller but it derives from QuaternionController.");
    return std::map<std::string, double>();
}

class ControllerServiceImpl final : public Controller::Service
{
    public:
        ControllerServiceImpl(Controller_* c) : controller(c), to_grpc(), from_grpc()
        {}
        grpc::Status set_parameters(grpc::ServerContext* , const SetParametersRequest* request, SetParametersResponse* reply) override
        {
            SetParametersResponse_ response = controller->set_parameters(request->parameters());
            controller->set_t0(request->t0());
            controller->set_dt(response.dt);
            reply->set_dt(response.dt);
            reply->set_date_of_first_callback(controller->get_date_of_next_call(request->t0()));
            to_grpc.copy_from_string_vector(response.setpoint_names, reply->mutable_setpoint_names());
            to_grpc.copy_from_string_vector(response.command_names, reply->mutable_command_names());
            switch(controller->get_angle_representation())
            {
                case AngleRepresentation_::QUATERNION:
                    reply->set_angle_representation(SetParametersResponse_AngleRepresentation::SetParametersResponse_AngleRepresentation_QUATERNION);
                    break;
                case AngleRepresentation_::EULER_321:
                    reply->set_angle_representation(SetParametersResponse_AngleRepresentation::SetParametersResponse_AngleRepresentation_EULER_321);
                    break;
                default:
                    THROW(__PRETTY_FUNCTION__, Exception, "Unknown angle representation");
                    break;
            }

            return grpc::Status::OK;
        }

        grpc::Status get_commands_quaternion(grpc::ServerContext* , const ControllerRequestQuaternion* request, ControllerResponse* reply) override
        {
            const QuaternionStates_ states = from_grpc.to_quaternion_states(request->states());
            const QuaternionStates_ dstates_dt = from_grpc.to_quaternion_states(request->dstates_dt());
            std::vector<double> setpoints;
            std::copy(request->setpoints().begin(), request->setpoints().end(), std::back_inserter(setpoints));
            const std::map<std::string,double> commands = controller->get_commands_quaternion(states, dstates_dt, setpoints);
            reply->mutable_commands()->insert(commands.begin(), commands.end());
            reply->set_next_call(controller->get_date_of_next_call(states.t));
            return grpc::Status::OK;
        }

        grpc::Status get_commands_euler_321(grpc::ServerContext* , const ControllerRequestEuler* request, ControllerResponse* reply) override
        {
            const EulerStates_ states = from_grpc.to_euler_states(request->states());
            const EulerStates_ dstates_dt = from_grpc.to_euler_states(request->dstates_dt());
            std::vector<double> setpoints;
            std::copy(request->setpoints().begin(), request->setpoints().end(), std::back_inserter(setpoints));
            const std::map<std::string,double> commands = controller->get_commands_euler_321(states, dstates_dt, setpoints);
            reply->mutable_commands()->insert(commands.begin(), commands.end());
            reply->set_next_call(controller->get_date_of_next_call(states.t));
            return grpc::Status::OK;
        }

    private:
        Controller_* controller;
        ToGrpc to_grpc;
        FromGrpc from_grpc;
};



void run(Controller_& controller)
{
    std::string server_address("0.0.0.0:9002");
    ControllerServiceImpl service(&controller);
    grpc::ServerBuilder builder;
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;
    server->Wait();
}

