# To use this image, supposing you have a PID.cpp source file in the current directory, use:
# docker build -t xdyn-controller-cpp -f Dockerfile.cpp ..
# docker run -it --rm -v $(pwd):/work -w /work xdyn-controller-cpp PID.cpp

FROM debian:bullseye-slim
RUN apt-get update --fix-missing -yq && \
    apt-get install --yes --no-install-recommends \
        make \
        cmake \
        build-essential \
        g++ \
        gcc \
        libabsl-dev \
        libbz2-dev \
        libc-ares-dev \
        libssl-dev \
        ninja-build \
        ca-certificates \
        wget \
        libgrpc++-dev \
        libgrpc10 \
        libgrpc++1 \
        libgrpc-dev \
        libprotobuf-dev \
        libprotoc-dev \
        protobuf-compiler-grpc

RUN wget --quiet https://github.com/google/googletest/archive/release-1.8.1.tar.gz -O googletest.tar.gz && \
    mkdir -p /opt && \
    mkdir -p /opt/googletest && \
    tar -xf googletest.tar.gz --strip 1 -C /opt/googletest && \
    rm -rf googletest.tar.gz

ADD proto/controller.proto /

WORKDIR /src
ADD examples/controllers/cpp/CMakeLists.txt\
    examples/controllers/cpp/Controller.cpp\
    examples/controllers/cpp/Controller.hpp\
    examples/controllers/cpp/FromGrpc.cpp\
    examples/controllers/cpp/FromGrpc.hpp\
    examples/controllers/cpp/ToGrpc.cpp\
    examples/controllers/cpp/ToGrpc.hpp\
    /src/
RUN chmod -R a+rw /src
RUN echo "#!/bin/sh" > /run.sh \
 && echo "cp \$1 /src/" >> /run.sh \
 && echo "sed -i \"s/YOURCPPFILENAMEHERE/\$1/g\" CMakeLists.txt" >> /run.sh \
 && echo "mkdir -p /work/build" >> /run.sh \
 && echo "cd /work/build" >> /run.sh \
 && echo "cmake -Wno-dev \\" >> /run.sh \
 && echo "	    -G Ninja \\" >> /run.sh \
 && echo "	    -DCMAKE_BUILD_TYPE=Relase \\" >> /run.sh \
 && echo "	    -DCMAKE_INSTALL_PREFIX:PATH=/opt/grpc_demo \\" >> /run.sh \
 && echo "	    /src" >> /run.sh \
 && echo "cd /work/build && ninja && /work/build/./controller_server" >> /run.sh \
 && chmod a+x /run.sh

ENTRYPOINT ["/./run.sh"]
