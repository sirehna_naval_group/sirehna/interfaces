import csv
import math

with open('out.csv', newline='') as csvfile:
    csv_reader = csv.DictReader(csvfile)
    t = []
    psi = []
    for row in csv_reader:
        t = float(row['t'])
        x = float(row['x(TestShip)'])
        theory = 3.5*(math.cos((t+8)*0.089)+math.sin((t+8)*0.089))
        assert abs(x - theory) < 0.5
